# Tenzen Table Tennis
### An Open Source Unity Android Game Project
---

![d](https://img.youtube.com/vi/jyT4Mg_6ozs/maxresdefault.jpg)

## - Introduction:
This is my first game project I started for me to practice about **Game Design**, **Game Art** and **Programing Design Pattern**.

Since the basic structure and content of the game have been finished, I have released this game on Google Play and make this repository public to receive any comment and feedback 

(Please be gentle (ノД`)・゜・。)

Google Play:

https://play.google.com/store/apps/details?id=com.windroid.ttt

Demo Video: 

[![Tenzen Table Tennis Demo Video](https://img.youtube.com/vi/jyT4Mg_6ozs/0.jpg)](https://youtu.be/jyT4Mg_6ozs)

---
## - Game Design:

#### ・Story Setting:

There's a monkey Table Tennis Master named Mr. Mo, who love fruits and Table Tennis.

You, dreamed to be a professional table tennis player, are ready to accept the Table Tennis challenge from Mr. Mo...To feed him in a Table Tennis way!

#### ・Gameplay:
**Rules:**

* It's simple! Player have to hit the Circle button at the right timing when the fruits coming in front of the player. So that it bounce back to Mr. Mo for him to eat and get the score of you hit timing.
* Score will be added according to your timing, aim for the high score!
* Game Over when you missig the fruit until out of 3 life.
* Game will be more diffcult each next wave.

The following element of the game will be changed each time player hit and every wave:

* Tempo of the game
* Buttons' position
* Buttons' amount
* Fruits' speed
* Fruits' target destination
* Fruits' type (Each type of fruit behave differently)
    * Apple: Normal Behaviour
    * Green Apple: Add 1 life when you hit it sucessfully
    * Banana: Tricky one! No need to hit as it faking fly to you
    * ...

Also it comes with 3 different difficulty(+ 1 hidden hell difficulty) of courses for the player to choose with.

---

#### ・Mechanics:
**Spline Curved Paths**
![d](https://drive.google.com/uc?export=view&id=1c_JoMsPqaaql0oQr0Mowf8B3BzdvZS20)
In order to make the fruit movement looks smooth and predictable, all the movement are pre-created path which use Bezier curves to calculate, then make it follow the path.

**After Image Effect**
![d](https://drive.google.com/uc?export=view&id=1eW-6h0cVgJJTU7AxzLkn2VgPby6goWko)
Since it is a simple model, I do it with baking a new mesh with transparecy controlled and managed in a list. 

**Scriptable Object Game Data**
![d](https://drive.google.com/uc?export=view&id=1iCKsc6ntlMA0ohipFLPMFDcZhNf2N0gZ)
For easier adding or editing data, data for every levels of wave and references of audio and particle are managed with Scriptable Object.

---

#### ・Game Art:

**Graphic:**
![d](https://drive.google.com/uc?export=view&id=1-F11zQeJl6YUqpZUocPORgv5xkUu6j1P)

**Art Style:**
Low Poly style with colorful material

Because of the simple,cute,casual story setting and gameplay.
Also, because of the mobile performance.

**Shading:**
Cel shading

Created toon-like shading with Shader Graph and applied model outline effect with URP Render Feature.
Game camera and UI camera is rendering differerntly with Camera Stacking, Game camera screen are adjusted with Post Processing.

**Music:**
Casual jungle music

Match with the game tempo and Mr. Mo monkey setting

---
## - Design Pattern:

I have used numbers of design pattern in this game.


#### ・Singleton:

Used in any Manager Class,used for manage global use of variable and reference. 

This is a design pattern that known for its bad of dependency, gonna use Scriptable Object Reference pattern(https://youtu.be/raQ3iHhE_Kk) to (almost)replace it in the next project.

#### ・Object Pool:

Used in ParticleFXManager Class, used for store and reuse the same simple particle effect again and again.

The particle effect I used in this project are simple and easy to initialize, perfect for object pool.

#### ・Observer:

Only used in few classes with UniRx and UnityEvent.

Not too necessary in this project since there's not much constant checking status in this project. 

It should be replace most of the current code if I chose to use this pattern. But I just want to test the use of Observer pattern. :P

#### ・State:

Used in GameManager Class to manage current game state.

#### ・Subclass Sandbox:

Used in Classes with the fruits, used for create variation of fruit type and behavior.

#### ・Others:

Every wave setup data was saved in ScriptableObject, So that it is easy to add or edit the wave data in the editor. 

I extended transform part in the editor was used since there are many times I need to copy local and global position of obects.

---
## Planned To add if I have time:

* New game modes
* New fruit types
* Google Play Achievement support
* Online score ranking
* More cartoon-like animation for Mr. Mo
* more...

---
## Asset Credits:

* Mr. Mo (https://assetstore.unity.com/packages/3d/characters/mr-mo-character-57859)
* Simple Sky - Cartoon assets (https://assetstore.unity.com/packages/3d/simple-sky-cartoon-assets-42373)
* Low Poly Ground (https://assetstore.unity.com/packages/3d/environments/landscapes/low-poly-ground-61071)
* Low Poly Fruit Pickups (https://assetstore.unity.com/packages/3d/props/food/low-poly-fruit-pickups-98135)
* Low Poly's Pack vol. 2(https://assetstore.unity.com/packages/3d/environments/low-poly-s-pack-vol-2-46375)

Contact me if you found your content in this repository
 haven't been credited.
 
---
## Development Environmet:

#### Unity Version: 

Unity 2018.3.11f (EDIT: Updated to 2020.1.5f1 and started to use URP)

#### Development Time:
About 180+- hours（Including a little bit modeling, drawing and researching time） 

 ---
# **[日本語説明]**：
# Tenzen Table Tennis
### Unity Androidゲームの個人制作
---

![d](https://img.youtube.com/vi/jyT4Mg_6ozs/maxresdefault.jpg)

## - 前言：

この作品は自分がゲーム**デザイン**、**ゲームアート**と**プログラミングデザインパターン**についてを練習するために作った最初の試作品です。

基本のゲーム構成を完成したため、このプロジェクトのソースコードを公開し、Google Playにもリリースしている、ご意見やコメントは随時募集しています。

Google Play：

https://play.google.com/store/apps/details?id=com.windroid.ttt

ゲーム動画： 

[![Tenzen Table Tennis ゲーム動画](https://img.youtube.com/vi/jyT4Mg_6ozs/0.jpg)](https://youtu.be/jyT4Mg_6ozs)

---
## - ゲームデザイン：

#### ・ストーリー設定：

伝説の卓球の達人Mr. Moは森に住んでいる、果物が大好きな猿である。

そして、君はプロな卓球選手を目指している、熱い心を持つ少年である。
偶然に出会ったMr. Moがそんな君にあるチャレンジーを与えた。。。
それは「フルーツ卓球」である！

#### ・ゲームプレイ：
**ルール：**

* とりあえず、Mr. Moから投げてきた果物を跳ね返してください！
* タイミングを合わせて、画面の「〇」ボタンを押すことで目の前に来た果物を跳ね返って、Mr. Moに食べてさせることができます。
* 押すタイミングによって、得るスコアが変わる。高得点を狙ってください！
* 失敗したらライフが失う、ライフがなくなったらゲームオーバー！
* ウェイブが進行しているとだんだん難しくなります。

ゲームの進行によって以下の要素が変わる：

* ゲームのスピード
* 「〇」ボタンの位置
* 「〇」ボタンの数
* 果物のスピード
* 果物の落下点
* 果物の種類（種類によって挙動が変わる）
    * 赤いリンゴ：普通の挙動
    * 緑のリンゴ：ヒットしたらライフを回復する
    * バナナ：飛んでくるフリをしている、ヒットしたライフが減る
    * ...

さらに、ゲームは３つの難易度のコース（＋１つ隠れ地獄難易度コース）が自由に選べます。

---

#### ・一部のゲームメカニック:
**果物のスプライン曲線パス**
![d](https://drive.google.com/uc?export=view&id=1c_JoMsPqaaql0oQr0Mowf8B3BzdvZS20)
果物の移動をよりスムーズ、プレイヤーが予測可能にしたいので、すべての移動は
ベジェ曲線の方程式で計算した曲線に従って移動しています。

**残像エフェクト**
![d](https://drive.google.com/uc?export=view&id=1eW-6h0cVgJJTU7AxzLkn2VgPby6goWko)
簡単なモデルなので、時間間隔で自身のメッシュをもう一度ベイクしてリストで管理し、マテリアルの透明度を徐々に下げる方法を使いました。

**Scriptable Objectでゲームデータ管理**
![d](https://drive.google.com/uc?export=view&id=1iCKsc6ntlMA0ohipFLPMFDcZhNf2N0gZ)
よりデータを編集しやすいように、すべてのステージパラメーターと音声とパーティクルはScriptable Objectを使って管理されている。

---

#### - ゲームアート：

**グラフィック：**
![d](https://drive.google.com/uc?export=view&id=1-F11zQeJl6YUqpZUocPORgv5xkUu6j1P)

**スタイル:**
カラフルなローポリゴンスタイル

（シンプル、キュート、カジュアルなストーリー設定に合わせる、より見やすいとやりやすいのゲームプレイ）
（スマホの性能を考慮しているため、画質は低めで作られています）

**シェーディング:**
セルシェーディング

Shader Graphを使ってトゥーンライクのシェーダーを書きました。
モデルのアウトラインエフェクトはURPのRender FeatureでHLSLコードを使いました。
ゲームカメラとUIカメラはCamera Stackingを使って別々でレンダリングされている、ゲームカメラの画面はポストプロセスで調整しました。

**音楽：**
カジュアルなジャングル風の音楽

（森の猿の設定とゲームのテンポを合わせる）

---
## - デザインパターン：

このプロジェクトでいくつのデザインパターンを使っています。

#### ・シングルト ン (Singleton)：

グローバル的に使われている変数とレファレンスを管理するために、Managerクラスで使われています。

これは依存性が重いというデメリットだと知られているデザインパターン、次のプロジェクトは代わりにScriptableObjectレファレンスパターン (https://youtu.be/raQ3iHhE_Kk) で代用する予定。

#### ・オブジェクトプール (Object Pool)：

よく使うパーティクルエフェクトオブジェクトを保存と再利用するために、ParticleFXManagerで使われています。

このプロジェクトで使っているパーティクルエフェクトはシンプルで簡単に初期化するものなので、オブジェクトプールに最適です。

#### ・オブサーバ (Observer)：

少数のクラスでUniRXとUnityEventを使っています。

このプロジェクトにはコンスタントに状態の確認がないので、別にオブサーバが必要なわけではないし、それに本気でUniRXを使うならほぼ全部のコードを書き直しなければならないですが、ただ使ってみたいだけです。（笑）

#### ・ステート (State)：

ゲームの進行ステートを管理するために、GameManagerクラスで使われています。

#### ・サブクラスサンドボックス (Subclass Sandbox)：

違う果物の種類の挙動を作るために、果物のクラスで使われています。

果物の共通の部分をベースクラスとして、特別な挙動が必要な果物は子クラスで書いています。

#### ・他には：

ゲームで毎ウェイブで出る果物の種類、スピード、ボタンの位置など、簡単に追加や編集できるように、毎ウェイブのセットアップデータはScriptableObjectを使用しています。

今回のプロジェクトではオブジェクトのグローバルとロカール位置情報が頻繁に使用するため、Unityエディター拡張スクリプトを書き、必要な機能を追加しています。

---
## 時間があれば追加予定の機能：

* 新しいゲームモード
* 新しい果物の種類
* Google Play実績の実装
* オンラインランキング
* Mr.Moに新しいアニメーションの追加
* ...

---
## 使用したモデル：

* Mr. Mo (https://assetstore.unity.com/packages/3d/characters/mr-mo-character-57859)
* Simple Sky - Cartoon assets (https://assetstore.unity.com/packages/3d/simple-sky-cartoon-assets-42373)
* Low Poly Ground (https://assetstore.unity.com/packages/3d/environments/landscapes/low-poly-ground-61071)
* Low Poly Fruit Pickups (https://assetstore.unity.com/packages/3d/props/food/low-poly-fruit-pickups-98135)
* Low Poly's Pack vol. 2(https://assetstore.unity.com/packages/3d/environments/low-poly-s-pack-vol-2-46375)

---
## 開発環境：

#### Unityバージョン：

Unity 2018.3.11f (EDIT: 2020.1.5f1にアップデートし、URPに変えました)

#### 開発時間：
おおよそ 180+- 時間（少しのモデリングと画像編集とデザインパターンのリサーチ時間を含めて）