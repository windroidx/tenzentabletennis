void LWRPLightingFunction_float (float3 WorldPos, out float3 Direction, out float3 Color, out float ShadowAttenuation)
{
/*    #if SHADERGRAPH_PREVIEW
   
      // Hardcoded data, used for the preview shader inside the graph
      // where light functions are not available
      Direction = float3(-0.5, 0.5, -0.5);
      Color = float3(1, 1, 1);
      ShadowAttenuation = 0.4;

   #else
   
      // Actual light data from the pipeline
      // Light light = GetMainLight(GetShadowCoord(GetVertexPositionInputs(ObjPos)));
      // Direction = light.direction;
      // Color = light.color;
      // ShadowAttenuation = light.shadowAttenuation;
	
	#if SHADOWS_SCREEN
		float4 clipPos = TransformWorldToHClip(WorldPos);
		float4 shadowCoord = ComputeScreenPos(clipPos);
	#else
		float4 shadowCoord = TransformWorldToShadowCoord(WorldPos);
	#endif
	Light mainLight = GetMainLight(shadowCoord);
	Direction = mainLight.direction;
	Color = mainLight.color;
	ShadowAttenuation = mainLight.shadowAttenuation;
	  
   #endif */
   
    #if SHADERGRAPH_PREVIEW
		//Hardcoded data, used for the preview shader inside the graph
		//where light functions are not available
		Direction = float3(0.5, 0.5, 0);
		Color = 1;
		ShadowAttenuation = 0.4;

   #else
	  //Actual light data from the pipeline
      Light light = GetMainLight();
      Direction = light.direction;
      Color = light.color;
      ShadowAttenuation = light.shadowAttenuation;
      
   #endif
}