﻿using Pixelplacement;
using UnityEngine;

// Manager reference for scene object
public class RefManager : Singleton<RefManager>
{
    public Transform[] ReflectionPoints;
    public Transform[][][] HitPoints;
    public Transform[][][] ReturnHitPoints;
    public Transform HitButton;
    public ButtonController[] HitButtons;
    public MrMoController MrMo;
    public CameraController CameraController;

    private int maxDir;

    private void Awake()
    {
        maxDir = System.Enum.GetValues(typeof(DirID)).Length;
        LoadHitPoints();
        LoadHitButtons();
        LoadMrMo();
        LoadCameraController();
    }

    private void LoadHitPoints()
    {
        // Load reflection points
        Transform refPt = transform.root.Find("Ball Reflection Points");
        ReflectionPoints = new Transform[maxDir];
        for (int i = 0; i < maxDir; i++)
        {
            string dirString = System.Enum.GetName(typeof(DirID), i);
            ReflectionPoints[i] = refPt.Find("Ball Reflection Point " + dirString);
        }

        // Load hit points
        HitPoints = new Transform[maxDir][][];
        ReturnHitPoints = new Transform[maxDir][][];

        for (int i = 0; i < maxDir; i++)
        {
            int num = 0;
            int returnNum = 0;
            foreach (Transform t in ReflectionPoints[i])
            {
                if (t.name.Contains("Return"))
                {
                    returnNum++;
                }
                else
                {
                    num++;
                }
            }
            HitPoints[i] = new Transform[num][];
            ReturnHitPoints[i] = new Transform[returnNum][];
            num = 0;
            returnNum = 0;

            foreach (Transform t in ReflectionPoints[i])
            {
                if (t.name.Contains("Return"))
                {
                    ReturnHitPoints[i][returnNum] = new Transform[System.Enum.GetValues(typeof(SplineType)).Length];
                    ReturnHitPoints[i][returnNum][(int)SplineType.Shot] = t.Find(SplineType.Shot.ToString());
                    ReturnHitPoints[i][returnNum][(int)SplineType.Back] = t.Find(SplineType.Back.ToString());
                    returnNum++;
                }
                else
                {
                    HitPoints[i][num] = new Transform[System.Enum.GetValues(typeof(SplineType)).Length];
                    HitPoints[i][num][(int)SplineType.Shot] = t.Find(SplineType.Shot.ToString());
                    HitPoints[i][num][(int)SplineType.Back] = t.Find(SplineType.Back.ToString());
                    num++;
                }
            }
        }
    }

    private void LoadHitButtons()
    {
        HitButton = transform.root.Find("Hit Buttons");
        HitButtons = new ButtonController[maxDir];
        for (int i = 0; i < maxDir; i++)
        {
            string dirString = System.Enum.GetName(typeof(DirID), i);
            HitButtons[i] = HitButton.transform.Find("Hit Button " + dirString).GetComponent<ButtonController>();
        }
    }

    private void LoadMrMo()
    {
        if (MrMo == null) MrMo = GameObject.FindGameObjectWithTag("MrMo").GetComponent<MrMoController>();
    }

    private void LoadCameraController()
    {
        CameraController = Camera.main.GetComponent<CameraController>();
    }
}