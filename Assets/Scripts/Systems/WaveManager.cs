﻿using Pixelplacement;
using RotaryHeart.Lib.SerializableDictionary;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BallTypeObjDict : SerializableDictionaryBase<BallType, GameObject> { }

[System.Serializable]
public class DifficultyWaveDataDict : SerializableDictionaryBase<Difficulty, WaveDataStruct> { }

// It is needed to declare a struct for serialize array
[System.Serializable]
public struct WaveDataStruct
{
    public WaveData[] waveDatas;
}

public class WaveManager : Singleton<WaveManager>
{
    /// <summary>
    /// Data for each ball
    /// </summary>
    public struct BallData
    {
        public Spline[] splines;
        public DirID dirID;
        public int hitPtID;
        public BallType ballType;
        public float duration;
        public float delay;
    }

    // References and value to set in inspector
    public int firstWave = 1;
    public Transform ballSpawn;
    public DifficultyWaveDataDict waveDataDict;
    public BallTypeObjDict ballPrefabDict;


    #region Private Variable
    private BallData ballData = new BallData()
    {
        hitPtID = 0,
        splines = new Spline[System.Enum.GetValues(typeof(SplineType)).Length]
    };
    private GameObject ball;
    private Coroutine waveCoroutine = null; 
    #endregion

    /// <summary>
    /// Start wave that begin with desired wave
    /// This is the loop cycle of every wave
    /// </summary>
    /// <param name="currentWave">First wave to start</param>
    private IEnumerator StartWave(int currentWave, Difficulty difficulty)
    {
        Debug.Log("Starting Wave " + currentWave);

        int currentWaveId = currentWave - 1; // Wave ID for waveData array

        ScoreManager.Instance.ResetHitNShot(); // Reset hit count for next wave;

        // Enable/disable button if the dirReq in waveData was changed
        // Buttons will not be enabled if they are already enabled
        EnableButtons(waveDataDict[difficulty].waveDatas[currentWaveId].dirReq);
        DisableUnusedButtons(waveDataDict[difficulty].waveDatas[currentWaveId].dirReq);

        // Loop in the wave before getting enough target hit to next wave
        while (ScoreManager.Instance.Hit < waveDataDict[difficulty].waveDatas[currentWaveId].hitToNextWave && InGame() ||
            waveDataDict[difficulty].waveDatas[currentWaveId].hitToNextWave == 0 && InGame())
        {
            // Set Mr. Mo animation play speed
            RefManager.Instance.MrMo.SetAnimSpeed(waveDataDict[difficulty].waveDatas[currentWaveId].animSpeed);

            // Generate random ball data
            GenerateBallData(
                waveDataDict[difficulty].waveDatas[currentWaveId].duration,
                waveDataDict[difficulty].waveDatas[currentWaveId].delay,
                waveDataDict[difficulty].waveDatas[currentWaveId].dirReq,
                waveDataDict[difficulty].waveDatas[currentWaveId].hitPtPatternRange,
                waveDataDict[difficulty].waveDatas[currentWaveId].returnHitPtPatternRange,
                waveDataDict[difficulty].waveDatas[currentWaveId].ReqShotToRandPattern,
                waveDataDict[difficulty].waveDatas[currentWaveId].ballTypePercentage);

            // Move buttons to generated pattern positions
            MoveButtons();

            // Wait for seconds that current wave need before spawn ball
            yield return new WaitForSeconds(waveDataDict[difficulty].waveDatas[currentWaveId].waitTime);

            // Reset last ball timing rating to none
            ScoreManager.Instance.ResetBallRating();

            // Spawn ball with generated ball data
            if (InGame()) SpawnBall(ballData);

            // Wait until the ball get destroyed
            while (ball != null) yield return new WaitForEndOfFrame();

            if (waveDataDict[difficulty].waveDatas[currentWaveId].hitToNextWave == 0) break;
        }

        // Start next wave if exist and in game mode
        if (currentWave < waveDataDict[difficulty].waveDatas.Length && InGame())
        {
            currentWave++;
            waveCoroutine = StartCoroutine(StartWave(currentWave, difficulty));
        }
        // Start the same wave if next wave not exist and in game mode
        else if (currentWave >= waveDataDict[difficulty].waveDatas.Length && InGame())
        {
            waveCoroutine = StartCoroutine(StartWave(currentWave, difficulty));
        }
        // Reset MrMo animation speed when game mode end
        else
        {
            RefManager.Instance.MrMo.SetAnimSpeed(1.0f);
        }
    }

    private void Awake()
    {
        if (ballPrefabDict == null)
        {
            Debug.LogError("ballPrefabDict not found in WaveManager");
        }
        if (ballSpawn == null)
        {
            Debug.LogWarning("ballSpawn not found,trying to find with name [Ball Spawn]");
            ballSpawn = transform.root.Find("Ball Spawn");
        }
    }

    //private void Start()
    //{
    //    GameManager.Instance.gameStateMachine.OnStateEntered.AddListener(StartEndlessMode);
    //}

    public void StartEndlessMode(Difficulty difficulty)
    {
        if (waveCoroutine != null) StopCoroutine(waveCoroutine);
        if (ball != null) Destroy(ball);
        AudioManager.Instance.PlayBgm("Game");
        GUIManager.Instance.HideAll();
        RefManager.Instance.CameraController.MoveTo(GameState.EndlessMode, .8f, 0f,
            () =>
            {
                GUIManager.Instance.SoloHUD();
                GUIManager.Instance.hudController.UpdateHUD(0, 
                    GameManager.Instance.HighScore[(int)GameManager.Instance.CurrentDifficulty],
                    0);
                GameManager.Instance.ResetGame();
                waveCoroutine = StartCoroutine(StartWave(firstWave,difficulty));
            }
            );
    }

    #region Private Method

    //private void StartEndlessMode(GameObject stateMachineObj)
    //{
    //    if (stateMachineObj.name == GameState.EndlessMode.ToString())
    //    {
//          if (waveCoroutine != null) StopCoroutine(waveCoroutine);
//          if (ball != null) Destroy(ball);
//          AudioManager.Instance.PlayBgm("Game");
//          GUIManager.Instance.HideAll();
//          RefManager.Instance.cameraController.MoveTo(GameState.EndlessMode, .8f, 0f,
//              () =>
//              {
//                GUIManager.Instance.SoloHUD();
//                GameManager.Instance.ResetGame();
//                ScoreManager.Instance.UpdateHighScore();
//                waveCoroutine = StartCoroutine(StartWave(firstWave));
//              }
//            );
    //    }
    //}

    /// <summary>
    /// Spawn ball with specific parameters.
    /// </summary>
    /// <param name="duration">Duration of the ball spline.(smaller get faster,larger get slower)</param>
    /// <param name="delay">Delay of the ball spline.(delay time before shot)</param>
    /// <param name="dirReq">Required direction for the wave</param>
    private void SpawnBall(BallData ballData)
    {
        ball = Instantiate(ballPrefabDict[ballData.ballType], ballSpawn) as GameObject;
        ball.GetComponent<BallBase>().Shot(ballData);
        ScoreManager.Instance.AddShot();
    }

    // Generate random ball data for SpawnBall() to use
    private BallData GenerateBallData(float duration, float delay, DirID[] dirReq, Vector2Int hitPtPatternRange, Vector2Int returnHitPtPatternRange, int ReqShotToRandPattern, BallTypeFloatDict ballTypePercentage)
    {
        ballData.duration = duration;
        ballData.delay = delay;

        // Random choose the ball type in different probability which set in wave data
        float totalPer = 0;
        foreach (float per in ballTypePercentage.Values)
        {
            totalPer += per;
        }
        float rand = Random.Range(0, totalPer);
        foreach (KeyValuePair<BallType, float> pair in ballTypePercentage)
        {
            rand -= pair.Value;
            if (rand <= 0)
            {
                ballData.ballType = pair.Key;
                break;
            }
        }

        // Random choose a direction from current required direction
        DirID randDir = dirReq[Random.Range(0, dirReq.Length)];
        ballData.dirID = randDir;
        Debug.Log("Random DIR: " + randDir);

        // Random choose a hit point ID when the required shot is reached
        if (ballData.ballType == BallType.Return)
        {
            ballData.hitPtID = Random.Range(returnHitPtPatternRange.x - 1, returnHitPtPatternRange.y);
        }
        else if (Mathf.Repeat(ScoreManager.Instance.Shot, ReqShotToRandPattern) == 0)
        {
            ballData.hitPtID = Random.Range(hitPtPatternRange.x - 1, hitPtPatternRange.y);
        }

        // Get the hit point reference and splines with the hit point ID and direction
        Transform[] hitPoint;
        if (ballData.ballType == BallType.Return)
        {
            hitPoint = RefManager.Instance.ReturnHitPoints[(int)randDir][ballData.hitPtID];
        }
        else
        {
            hitPoint = RefManager.Instance.HitPoints[(int)randDir][ballData.hitPtID];
        }

        ballData.splines[(int)SplineType.Shot] = hitPoint[(int)SplineType.Shot].GetComponent<Spline>();
        ballData.splines[(int)SplineType.Back] = hitPoint[(int)SplineType.Back].GetComponent<Spline>();

        return ballData;
    }

    private void EnableButtons(DirID[] dirReq)
    {
        foreach (DirID dir in dirReq)
        {
            RefManager.Instance.HitButtons[(int)dir].EnableButton();
        }
    }

    private void DisableButtons(DirID[] dirIDs)
    {
        foreach (DirID dir in dirIDs)
        {
            RefManager.Instance.HitButtons[(int)dir].DisableButton();
        }
    }

    private void DisableUnusedButtons(DirID[] dirReqs)
    {
        List<DirID> unusedButtons = new List<DirID>();

        foreach (DirID dirID in System.Enum.GetValues(typeof(DirID)))
        {
            bool isContain = false;
            foreach (DirID dirReq in dirReqs)
            {
                if (dirReq == dirID)
                {
                    isContain = true;
                    break;
                }
            }

            if (!isContain)
            {
                unusedButtons.Add(dirID);
            }
        }
        foreach (DirID unusedButton in unusedButtons)
        {
            RefManager.Instance.HitButtons[(int)unusedButton].DisableButton();
        }
    }

    private void MoveButtons()
    {
        foreach (ButtonController hitButton in RefManager.Instance.HitButtons)
        {
            hitButton.MoveButon(RefManager.Instance.HitPoints[(int)hitButton.dirID][ballData.hitPtID][(int)SplineType.Shot].position);
        }
    }

    private bool InGame()
    {
        return GameManager.Instance.GameState != GameState.Title &&
            GameManager.Instance.GameState != GameState.Result;
    }

    #endregion Private Method
}