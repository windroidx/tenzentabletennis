﻿using Pixelplacement;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

[RequireComponent(typeof(AudioSource))]
public class AudioManager : Singleton<AudioManager>
{
    public float sfxVolume = .5f;
    public float bgmVolume = .5f;
    public AudioMixerGroup sfxMixer;
    public AudioMixerGroup bgmMixer;

    public AudioDictData sfxDictData;
    public AudioDictData bgmDictData;

    private AudioSource sfxAudioSrc;
    private AudioSource bgmAudioSrc;

    // Use this for initialization
    private void Awake()
    {
        sfxAudioSrc = GetComponents<AudioSource>()[0];
        bgmAudioSrc = GetComponents<AudioSource>()[1];
        bgmAudioSrc.loop = true;
        sfxAudioSrc.outputAudioMixerGroup = sfxMixer;
        bgmAudioSrc.outputAudioMixerGroup = bgmMixer;
    }

    private void Start()
    {
        ApplyBgmVolume(GameManager.Instance.BgmVolume);
        ApplySfxVolume(GameManager.Instance.SfxVolume);
    }

    #region Public Method

    public void PlaySfx(string sfxName)
    {
        AudioClip sfx = sfxDictData.Get(sfxName);
        if (sfx == null)
        {
            Debug.LogWarning("No sfx " + sfxName + " was found!");
        }
        else
        {
            Debug.Log("Play SFX " + sfxName);
            sfxAudioSrc.PlayOneShot(sfx);
        }
    }

    public void PlayBgm(string bgmName, bool loop = true)
    {
        AudioClip bgm = bgmDictData.Get(bgmName);
        if (bgm == null)
        {
            Debug.LogWarning("No bgm " + bgmName + " was found!");
            return;
        }

        bgmAudioSrc.loop = loop;
        if (bgmAudioSrc.isPlaying)
        {
            StopBgm(() =>
            {
                bgmAudioSrc.clip = bgm;
                bgmAudioSrc.Play();
            }
            );
        }
        else
        {
            bgmAudioSrc.clip = bgm;
            bgmAudioSrc.Play();
        }
    }

    public void ApplySfxVolume(float volume)
    {
        sfxVolume = volume;
        sfxAudioSrc.volume = sfxVolume;
    }

    public void ApplyBgmVolume(float volume)
    {
        bgmVolume = volume;
        bgmAudioSrc.volume = bgmVolume;
    }

    public void SaveVolume()
    {
        GameManager.Instance.SfxVolume = sfxVolume;
        GameManager.Instance.BgmVolume = bgmVolume;
    }

    public void StopBgm(System.Action onComplete = null)
    {
        if (bgmAudioSrc.isPlaying)
            Tween.Volume(bgmAudioSrc, 0, 1f, 0f, null, Tween.LoopType.None, null, () =>
            {
                bgmAudioSrc.Stop();
                bgmAudioSrc.volume = bgmVolume;
                onComplete?.Invoke();
            });
    }

    public void PauseBgm()
    {
        if (bgmAudioSrc.isPlaying && bgmAudioSrc.clip != null) bgmAudioSrc.Pause();
        else if (!bgmAudioSrc.isPlaying && bgmAudioSrc.clip != null) bgmAudioSrc.UnPause();
    }

    #endregion Public Method
}