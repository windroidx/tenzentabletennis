﻿using Pixelplacement;
using UnityEngine;

public class ParticleFXManager : Singleton<ParticleFXManager>
{
    public PrefabDictData particleData;

    private readonly PrefabDict pool = new PrefabDict();

    public void SpawnFX(string name, Transform m_transform)
    {
        GameObject particle = particleData.Get(name);
        if (particle == null)
        {
            Debug.LogWarning("No particle " + name + " was found!");
            return;
        }

        if (pool.ContainsKey(name))
        {
            pool[name].transform.position = m_transform.position;
            pool[name].transform.rotation = m_transform.rotation;
            //pool[name].transform.SetParent(transform);
            pool[name].SetActive(true);
            Debug.Log("Enabled existing fx " + name + " from FXPool");
        }
        else
        {
            pool.Add(name, Instantiate(particle, m_transform) as GameObject);
            //pool[name].transform.SetParent(transform);
            Debug.Log("No existing fx was found, added " + name + " into FXPool");
        }
    }
}