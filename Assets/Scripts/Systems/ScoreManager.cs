﻿using Pixelplacement;
using RotaryHeart.Lib.SerializableDictionary;
using UnityEngine;
using UniRx;

[System.Serializable]
public class RatingScoreDict : SerializableDictionaryBase<Timing, int> { }

public class ScoreManager : Singleton<ScoreManager>
{
    private Subject<Timing> ratingSubject = new Subject<Timing>();
    public System.IObservable<Timing> OnRated
    {
        get { return ratingSubject; }
    }

    private int _score;
    public int Score {
        private set{
            _score = Mathf.Clamp(value, 0, 99999);
        }
        get {return _score; }
    }

    //public int HighScore { private set; get; } = 0;
    public int Combo { private set; get; } = 0;
    public int MaxCombo { private set; get; } = 0;

    /// <summary>
    /// Shot count for each wave
    /// </summary>
    public int Shot { private set; get; } = 0;

    /// <summary>
    /// Success hit count for each wave
    /// </summary>
    public int Hit { private set; get; } = 0;

    /// <summary>
    /// Total hit count for every rating
    /// </summary>
    public int[] TotalHit { private set; get; } = { 0 };

    public Timing BallRating { private set; get; } = Timing.None;

    /// <summary>
    /// Dictionary to store the rating score setting
    /// </summary>
    public RatingScoreDict ratingScoreDict;

    private void Awake()
    {
        // create hit count from Bad to Miss
        TotalHit = new int[(int)Timing.Miss + 1];
        ResetScore();
    }

    #region Public Method

    /// <summary>
    /// Set the ball rating for current round and add score according to the timing
    /// received from button controller
    /// </summary>
    /// <param name="timing">Ball timing of current round</param>
    public void ScoreRating(Timing timing)
    {
        if (BallRating == Timing.None)
        {
            BallRating = timing;
            ratingSubject.OnNext(timing);

            Score += ratingScoreDict[timing];
            TotalHit[(int)timing]++;
            if (timing != Timing.Miss)
            {
                Hit++;
                Combo++;
                MaxCombo = Combo > MaxCombo ? Combo : MaxCombo;
            }
            else Combo = 0;

            GUIManager.Instance.hudController.UpdateHUD(Score, 
                GameManager.Instance.HighScore[(int)GameManager.Instance.CurrentDifficulty], 
                TotalHit[(int)Timing.Miss]);
        }
    }

    /// <summary>
    /// Reset ball rating from last round to None to prepare for next round
    /// </summary>
    public void ResetBallRating()
    {
        BallRating = Timing.None;
    }

    public void ResetHitNShot()
    {
        Hit = 0;
        Shot = 0;
    }

    /// <summary>
    /// Reset score data in the current game(this is for game manager Resetgame())
    /// </summary>
    public void ResetScore()
    {
        ResetHitNShot();
        ResetBallRating();
        Combo = 0;
        MaxCombo = 0;
        Score = 0;
        System.Array.Clear(TotalHit,0,TotalHit.Length);
    }

    public void AddShot()
    {
        Shot++;
    }

    #endregion Public Method

    private void OnDisable()
    {
        ratingSubject.OnCompleted();
    }
}