﻿public enum DirID
{
    Left = 0,
    Center = 1,
    Right = 2,
    CenterLeft = 3,
    CenterRight = 4
}

public enum SplineType
{
    Shot = 0,
    Back = 1
}

public enum BallType
{
    Normal = 0,
    Recover = 1, // green apple
    Return = 2, // banana
}

public enum Timing
{
    Bad = 0,
    Good = 1,
    Cool = 2,
    Miss = 3,
    None = 4,
    //Success = Bad | Good | Cool
}

public enum Difficulty
{
    Easy = 0,
    Normal = 1,
    Hard = 2,
    Hell = 3 //NOTE: Add hell difficulty or not?
}

public enum GameState
{
    Title = 0,
    TimeAttackMode = 1,
    EndlessMode = 2,
    HonkiMode = 3,
    Pause = 4,
    Result = 5,
}

//public enum Rank
//{
//    BadPlayer = 0,
//    NormalPlayer = 1,
//}

