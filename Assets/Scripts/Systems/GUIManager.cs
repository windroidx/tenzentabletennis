﻿using Pixelplacement;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GUIManager : Singleton<GUIManager>
{
    public HUDController hudController;
    public ResultController resultController;
    
    [SerializeField]
    private DisplayObject intro;
    
    [SerializeField]
    private DisplayObject title;

    [SerializeField]
    private DisplayObject hud;

    [SerializeField]
    private DisplayObject pauseMenu;
    
    [SerializeField]
    private DisplayObject result;

    [SerializeField]
    private GameObject hitButton;

    private void Awake()
    {
        if (hud == null) hud = transform.Find("HUD").GetComponent<DisplayObject>();
        if (title == null) title = transform.Find("Title").GetComponent<DisplayObject>();
        if (title == null) pauseMenu = transform.Find("Pause Menu").GetComponent<DisplayObject>();
        if (title == null) result = transform.Find("Result").GetComponent<DisplayObject>();
        if (title == null) intro = transform.Find("Intro").GetComponent<DisplayObject>();
        if (hudController == null) hudController = hud.GetComponent<HUDController>();
        if (resultController == null) resultController = result.GetComponent<ResultController>();
        if (hitButton == null) hitButton = transform.root.Find("Hit Buttons").gameObject;

        //hudController.Awake();
        //resultController.Awake();
    }

    public void PlayIntro(System.Action onComplete = null)
    {
        Debug.Log("Play Intro");
        Tween.Color(intro.GetComponent<Image>(), Color.clear, 2f, 0f, null, Tween.LoopType.None, null, onComplete);
    }

    public void HideAll()
    {
        Debug.Log("Hided all GUI");
        hitButton.SetActive(false);
        title.HideAll();
    }

    public void SoloTitle()
    {
        Debug.Log("Solo Title");
        hitButton.SetActive(false);
        title.Solo();
    }

    public void SoloPauseMenu()
    {
        Debug.Log("Solo Pausu Menu");
        hitButton.SetActive(false);
        pauseMenu.Solo();
    }

    public void SoloHUD()
    {
        Debug.Log("Solo HUD and hitbutton");
        hitButton.SetActive(true);
        hud.Solo();
    }
    
    public void SoloResult()
    {
        Debug.Log("Solo Result");
        hitButton.SetActive(false);
        result.Solo();
    }
}