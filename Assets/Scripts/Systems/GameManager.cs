﻿using Pixelplacement;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{
    public float BgmVolume
    {
        get
        {
            return PlayerPrefs.GetFloat("bgmVolume", .5f);
        }
        set
        {
            PlayerPrefs.SetFloat("bgmVolume", value);
        }
    }

    public float SfxVolume
    {
        get
        {
            return PlayerPrefs.GetFloat("sfxVolume", .5f);
        }
        set
        {
            PlayerPrefs.SetFloat("sfxVolume", value);
        }
    }

    private int[] m_highScore = new int[System.Enum.GetValues(typeof(Difficulty)).Length];

    public int[] HighScore
    {
        get
        {
            return PlayerPrefsX.GetIntArray("HighScore", 0, System.Enum.GetValues(typeof(Difficulty)).Length);
        }
        set
        {
            PlayerPrefsX.SetIntArray("HighScore", value);
        }
    }

    private int[] m_highCombo = new int[System.Enum.GetValues(typeof(Difficulty)).Length];

    public int[] HighCombo
    {
        get
        {
            return PlayerPrefsX.GetIntArray("HighCombo", 0, System.Enum.GetValues(typeof(Difficulty)).Length);
        }
        set
        {
            PlayerPrefsX.SetIntArray("HighCombo", value);
        }
    }

    public GameState GameState
    {
        get
        {
            return (GameState)System.Enum.Parse(typeof(GameState), GameStateMachine.currentState.name);
        }
    }

    public StateMachine GameStateMachine;

    //HACK: switch from enum to StateMachine
    public Difficulty CurrentDifficulty = Difficulty.Normal;

#if UNITY_EDITOR
    public bool ClearPlayerPrefs = false;
#endif

    private void Awake()
    {
#if UNITY_EDITOR
        if (ClearPlayerPrefs) PlayerPrefs.DeleteAll();
#endif
        if (GameStateMachine == null)
        {
            Debug.LogWarning("Game State Machine object not found!\nTrying to find [Game State Machine] in child of GameManager");
            GameStateMachine = transform.Find("Game State Machine").GetComponent<StateMachine>();
        }
    }

    private void Start()
    {
        //TODO: Add intro
        //GUIManager.Instance.PlayIntro();
        StartUpTitle();
    }

    public void EnterEndlessMode(Difficulty difficulty)
    {
        AudioManager.Instance.StopBgm();
        SetGameState(GameState.EndlessMode);
        CurrentDifficulty = difficulty;
        WaveManager.Instance.StartEndlessMode(difficulty);
    }

    //TODO: make it more observer pattern(set all action and listener with UniRx)
    public void EnterTitle()
    {
        SetGameState(GameState.Title);
        AudioManager.Instance.StopBgm();
        GUIManager.Instance.HideAll();
        AudioManager.Instance.PlayBgm("Title");
        RefManager.Instance.CameraController.MoveTo(GameState.Title, .8f, 0f, GUIManager.Instance.SoloTitle);
    }

    public void StartUpTitle()
    {
        SetGameState(GameState.Title);
        AudioManager.Instance.PlayBgm("Title");
        GUIManager.Instance.SoloTitle();
    }

    public void EnterResult()
    {
        SetGameState(GameState.Result);

        if (ScoreManager.Instance.Score > HighScore[(int)CurrentDifficulty])
        {
            m_highScore[(int)CurrentDifficulty] = ScoreManager.Instance.Score;
            HighScore = m_highScore;
        }

        if (ScoreManager.Instance.MaxCombo > HighCombo[(int)CurrentDifficulty])
        {
            m_highCombo[(int)CurrentDifficulty] = ScoreManager.Instance.MaxCombo;
            HighCombo = m_highCombo;
        }

        AudioManager.Instance.PlayBgm("Result", false);
        GUIManager.Instance.SoloResult();
        GUIManager.Instance.resultController.UpdateResult();
    }

    public void PauseGame()
    {
        Time.timeScale = 0f;
        AudioManager.Instance.PauseBgm();
        GUIManager.Instance.SoloPauseMenu();
    }

    public void UnpauseGame()
    {
        GUIManager.Instance.SoloHUD();
        AudioManager.Instance.PauseBgm();
        Time.timeScale = 1f;
    }

    public void ResetGame()
    {
        GUIManager.Instance.hudController.ResetHearts();
        ScoreManager.Instance.ResetScore();
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    private void SetGameState(GameState state)
    {
        Debug.Log("Set GameState to " + state);
        GameStateMachine.ChangeState(state.ToString());
    }
}