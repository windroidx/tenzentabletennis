﻿using System;
using System.Collections;
using System.Collections.Generic;
using RotaryHeart.Lib.SerializableDictionary;
using UnityEngine;

[System.Serializable]
public class AudioDict : SerializableDictionaryBase<string, AudioClip> { }

[CreateAssetMenu(fileName = "AudioData", menuName = "AudioDictData")]
public class AudioDictData : ScriptableObject
{
    public AudioDict audioDict = new AudioDict();

    public AudioClip Get(string key)
    {
        if (!audioDict.ContainsKey(key))
        {
            // Debug.LogWarning("No audio " + key + " was found!");
            return null;
        }
        return audioDict[key];
    }
}