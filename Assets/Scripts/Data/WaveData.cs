﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RotaryHeart.Lib.SerializableDictionary;

[System.Serializable]
public class BallTypeFloatDict : SerializableDictionaryBase<BallType, float> { }

[CreateAssetMenu(fileName = "Wave", menuName = "WaveData")]
public class WaveData : ScriptableObject
{
    /// <summary>
    /// The wave ID for identify.
    /// </summary>
    public int waveID;

    /// <summary>
    /// Wait time for the next shot of ball.
    /// </summary>
    public float waitTime = 1.5f;

    /// <summary>
    /// Duration of the ball spline.(smaller get faster,larger get slower)
    /// </summary>
    public float duration = 1.5f;

    /// <summary>
    /// Delay of the ball spline.(delay time before shot)
    /// </summary>
    public float delay = 0.1f;

    /// <summary>
    /// Mr. Mo animation play speed
    /// </summary>
    public float animSpeed = 1.0f;

    /// <summary>
    /// Required direction for the wave
    /// </summary>
    public DirID[] dirReq = { DirID.Left, DirID.Center, DirID.Right };

    /// <summary>
    /// The probability of ball types avaiable to shoot
    /// </summary>
    public BallTypeFloatDict ballTypePercentage = 
        new BallTypeFloatDict { {BallType.Normal, 80f},{BallType.Recover, 10f},{BallType.Return, 10f} };

    /// <summary>
    /// Range of random hit points pattern to use in every directions(Form hit point x to hit point y)
    /// </summary>
    public Vector2Int hitPtPatternRange = new Vector2Int(1, 1);
    
    /// <summary>
    /// Range of random return hit points pattern to use in every directions(Form hit point x to hit point y)
    /// </summary>
    public Vector2Int returnHitPtPatternRange = new Vector2Int(1, 1);

    /// <summary>
    /// Require shot to randomly change hit point pattern in every directions
    /// </summary>
    public int ReqShotToRandPattern = 5;

    /// <summary>
    /// Required hit to enter next wave
    /// </summary>
    public int hitToNextWave = 10;
}
