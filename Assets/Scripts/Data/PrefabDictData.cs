﻿using System.Collections;
using System.Collections.Generic;
using RotaryHeart.Lib.SerializableDictionary;
using UnityEngine;

[System.Serializable]
public class PrefabDict : SerializableDictionaryBase<string, GameObject> { }

[CreateAssetMenu(fileName = "PrefabData", menuName = "PrefabDictData")]
public class PrefabDictData : ScriptableObject
{
    public PrefabDict prefabDict = new PrefabDict();

    public GameObject Get(string key)
    {
        if (!prefabDict.ContainsKey(key))
        {
            return null;
        }
        return prefabDict[key];
    }
}