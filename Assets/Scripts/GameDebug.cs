﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameDebug : MonoBehaviour {

#if UNITY_EDITOR
    // Update is called once per frame
    void Update()
    {
        Debug();
    }

    void Debug()
    {
        if (Input.GetKey(KeyCode.Alpha1))
        {
            SceneManager.LoadScene("Game");
        }
    } 
#endif
}
