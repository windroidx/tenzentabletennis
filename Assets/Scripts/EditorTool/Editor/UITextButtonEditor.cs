﻿using UnityEditor;
using UnityEditor.EventSystems;

// Make UITextButton component's inspector show public variable like normal component
// I can just use debug mode to do this without this script anyway :p
[CustomEditor(typeof(UITextButton))]
public class UITextButtonEditor : EventTriggerEditor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        base.OnInspectorGUI();
    }
}
