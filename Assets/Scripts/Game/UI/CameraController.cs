﻿using Pixelplacement;
using RotaryHeart.Lib.SerializableDictionary;
using UnityEngine;

[System.Serializable]
public class StateTransformDict : SerializableDictionaryBase<GameState, Transform> { }

public class CameraController : MonoBehaviour
{
    public StateTransformDict camPosDict;

    public void MoveTo(Transform trans, float duration = .8f, float delay = 0.0f)
    {
        Tween.Position(transform, trans.position, duration, delay);
        Tween.Rotation(transform, trans.rotation, duration, delay);
    }

    public void MoveTo(GameState state, float duration = .8f, float delay = 0.0f, System.Action endAction = null)
    {
        if (!camPosDict.ContainsKey(state))
        {
            Debug.LogWarning("The transform not exist in camera position dictionary");
            return;
        }
        Tween.Position(transform, camPosDict[state].position, duration, delay, 
            AnimationCurve.EaseInOut(0f,0f,duration,1f), Tween.LoopType.None, null, endAction);
        Tween.Rotation(transform, camPosDict[state].rotation, duration, delay);
    }
}