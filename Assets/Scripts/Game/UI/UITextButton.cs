﻿using UnityEngine;
using UnityEngine.EventSystems;

// Make UI text get lower when pointer down and back to orginal position when pointer up
public class UITextButton : EventTrigger
{
    public float moveSize = .01f;
    private bool isMoved;

    public override void OnPointerDown(PointerEventData eventData)
    {
        if (!isMoved)
        {
            transform.GetChild(0).position += Vector3.down * moveSize;
            isMoved = true;
        }
    }

    public override void OnPointerEnter(PointerEventData eventData)
    {
        if (!isMoved && eventData.pointerPress != null)
        {
            transform.GetChild(0).position += Vector3.down * moveSize;
            isMoved = true;
        }
    }

    public override void OnPointerUp(PointerEventData eventData)
    {
        if (isMoved)
        {
            transform.GetChild(0).position += Vector3.up * moveSize;
            isMoved = false;
        }
    }

    public override void OnPointerExit(PointerEventData eventData)
    {
        if (isMoved)
        {
            transform.GetChild(0).position += Vector3.up * moveSize;
            isMoved = false;
        }
    }
}