﻿using UnityEngine.EventSystems;

public class EnterGameButton : EventTrigger
{
    public Difficulty difficulty;
    public bool isRetry = false;

    public override void OnPointerClick(PointerEventData eventData)
    {
        if (!isRetry) GameManager.Instance.EnterEndlessMode(difficulty);
        else GameManager.Instance.EnterEndlessMode(GameManager.Instance.CurrentDifficulty);
    }
}
