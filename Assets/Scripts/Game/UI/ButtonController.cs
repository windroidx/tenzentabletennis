﻿using Pixelplacement;
using System.Collections;
using UnityEngine;

public class ButtonController : MonoBehaviour
{
    // Direction ID for this button
    public DirID dirID;

    // Parameters for setting button press effect
    public Color MouseOverColor = new Color(1f, 1f, 1f, .7f);

    public Color MouseDownColor = new Color(1f, 1f, 1f, 1f);
    public GameObject SuccessSprite;
    public GameObject RatingText; // The rating effect reference showed according to hit timing
    public Color SuccessColor = new Color(1f, 1f, 1f, 0f);

    // Sprite when different the ball inside different timing zone
    public Sprite BadSprite;
    public Sprite GoodSprite;
    public Sprite CoolSprite;
    public Sprite NoneSprite;
    public Sprite MissSprite;

    // Duration of button moving other position
    public const float MoveDuration = .8f;

    /// <summary>
    /// Button sprite reference (for changing sprite color and spite)
    /// </summary>
    private SpriteRenderer spriteRen;
#pragma warning disable 0108

    private Collider collider;

#pragma warning restore 0108

    /// <summary>
    /// Boolean to check is button sprite color changing
    /// </summary>
    private bool isChangingColor = false;
    private Color colorToChange;

    // Button pressUp effect color (Default same as orginal button color)
    private Color mouseExitColor;

    // Size of every buttons
    private Vector3 buttonSize;

    // Ball reference while ball entering button collider
    private BallBase ball;

    // The timing of the ball (return by children's collider)
    private Timing ballTiming = Timing.None;

    private void Awake()
    {
        collider = GetComponent<Collider>();
        spriteRen = GetComponent<SpriteRenderer>();
        mouseExitColor = spriteRen.color;
        buttonSize = transform.localScale;
        if (SuccessSprite == null)
        {
            Debug.LogWarning("successSprite not found,trying to load from Resources");
            SuccessSprite = Resources.Load<GameObject>("Prefabs/SuccessSprite");
        }

        // Start position use hit point pattern 1
        transform.position = RefManager.Instance.HitPoints[(int)dirID][0][0].position;
    }

#if UNITY_ANDROID || UNITY_IOS || UNITY_EDITOR

    private void Update()
    {
        TouchControl();
    }

#endif

#if UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX || UNITY_EDITOR

    private void OnMouseOver()
    {
        if (!Input.GetMouseButtonDown(0)) StartCoroutine(ChangeButtonColor(MouseOverColor));
    }

    private void OnMouseExit()
    {
        if (!Input.GetMouseButtonDown(0)) StartCoroutine(ChangeButtonColor(mouseExitColor));
    }

    private void OnMouseDown()
    {
        StartCoroutine(ChangeButtonColor(MouseDownColor));
        PressButton();
    }

#endif

    #region Public Method

    /// <summary>
    /// Spawn rating effect according to final timing in score manager
    /// and set rating in score manager
    /// </summary>
    public void SetRating(Timing timing)
    {
        if (ScoreManager.Instance.BallRating == Timing.None)
        {
            Instantiate(RatingText, transform.position + Vector3.up * .2f, RatingText.transform.rotation)
                .GetComponent<RatingText>()
                .SetRating(timing);
            AudioManager.Instance.PlaySfx(timing.ToString());
            ScoreManager.Instance.ScoreRating(timing);
        }
    }

    /// <summary>
    /// Receive ball and timing data from children collider
    /// </summary>
    /// <param name="_ball">Ball inside the collider</param>
    /// <param name="timing">Current ball timing</param>
    public void SetBallTiming(BallBase _ball, Timing timing)
    {
        ball = _ball;
        ballTiming = timing;

        if (ballTiming == Timing.Miss)
        {
            SetRating(Timing.Miss);
        }
        ChangeButtonSprite(timing);

        //TODO: Add better sfx for mrmo 
        //TODO: Add hitready effect
        //ParticleFXManager.Instance.SpawnFX("HitReady", _ball.transform);
    }

    public void EnableButton()
    {
        if (!gameObject.activeSelf)
        {
            Debug.Log("Enable button " + dirID);
            gameObject.SetActive(true);
            transform.localScale = Vector3.zero;
            Tween.LocalScale(transform, buttonSize, 1f, .1f);
        }
    }

    public void DisableButton()
    {
        if (gameObject.activeSelf)
        {
            Debug.Log("Disable button " + dirID);
            Tween.LocalScale(transform, Vector3.zero, .7f, .1f, null, Tween.LoopType.None, null, () =>
            {
                gameObject.SetActive(false);
            });
        }
    }

    public void MoveButon(Vector3 targetPos)
    {
        if (gameObject.activeSelf && targetPos != transform.position)
        {
            Tween.Position(gameObject.transform, targetPos, MoveDuration, .1f, Tween.EaseInOut);
            Debug.Log("Moved Button " + dirID);
        }
    }

    #endregion Public Method

    #region Private Method

    private IEnumerator ChangeButtonColor(Color color, float duration = .15f, float delay = 0.0f)
    {
        
        if (color != colorToChange)
        {
            colorToChange = color;
            yield return new WaitUntil(() => !isChangingColor);
            isChangingColor = true;
            Tween.Color(spriteRen, colorToChange, duration, delay, null, Tween.LoopType.None,
                null, () => { isChangingColor = false; });
        }
    }

    private void ChangeButtonSprite(Timing timing)
    {
        switch (timing)
        {
            case Timing.Bad:
                spriteRen.sprite = BadSprite;
                break;

            case Timing.Good:
                spriteRen.sprite = GoodSprite;
                break;

            case Timing.Cool:
                spriteRen.sprite = CoolSprite;
                break;

            case Timing.None:
                spriteRen.sprite = NoneSprite;
                break;

            case Timing.Miss:
                spriteRen.sprite = MissSprite;
                break;

            default:
                Debug.LogWarning("Unrecognized rating was rated");
                break;
        }
    }

    private void PressButton()
    {
        // Hit when the ball is not inside any timing zone
        if (ball == null && ScoreManager.Instance.BallRating == Timing.None)
        {
            SetRating(Timing.Miss);
        }
        // Hit when the ball inside any timing zone and in correct condition
        if (ball != null && dirID == ball.dirID && ScoreManager.Instance.BallRating != Timing.Miss)
        {
            GameObject successSprite =
                Instantiate(SuccessSprite, transform.position + Vector3.forward * .01f, transform.rotation) as GameObject;
            Tween.Color(successSprite.GetComponent<SpriteRenderer>(), SuccessColor, .5f, .1f, null, Tween.LoopType.None, null,
                () => Destroy(successSprite));

            SetRating(ballTiming);
            ParticleFXManager.Instance.SpawnFX("HitSuccess", transform);
            ball.Back();
            Debug.Log(dirID + " backed!");
        }
    }

#if UNITY_ANDROID || UNITY_IOS || UNITY_EDITOR

    private void TouchControl()
    {
        if (Input.touchCount > 0)
        {
            if (Input.touches[0].phase == TouchPhase.Began)
            {
                Ray touchRay = Camera.main.ScreenPointToRay(Input.touches[0].position);

                if (Physics.Raycast(touchRay, out RaycastHit touchHit, LayerMask.NameToLayer("UI")))
                {
                    if (touchHit.collider == collider)
                    {
                        StartCoroutine(ChangeButtonColor(MouseDownColor));
                        PressButton();
                    }
                }
            }
            if (Input.touches[0].phase == TouchPhase.Ended)
            {
                StartCoroutine(ChangeButtonColor(mouseExitColor));
            }
        }
    }

#endif

    #endregion Private Method
}