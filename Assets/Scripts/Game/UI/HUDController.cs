﻿using NaughtyAttributes;
using Pixelplacement;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HUDController : MonoBehaviour
{
    public float tweenDuration = .5f;
    public float tweenDelay = .1f;
    public int maxHeart = 3;

    private int currentHeart;
    private TextMeshProUGUI scoreText;
    private TextMeshProUGUI highScoreText;

    private Image[] hudHearts;

    private int m_lastMiss = 0;

    private readonly Color transparent = new Color(1.0f, 1.0f, 1.0f, 0.0f);

    public void Awake()
    {
        currentHeart = maxHeart;
        hudHearts = new Image[currentHeart];
        for (int i = 1; i <= maxHeart; i++)
        {
            hudHearts[i - 1] = transform.Find("Life Counter").Find("Heart " + i).gameObject.GetComponent<Image>();
        }
        if (scoreText == null) scoreText = transform.Find("Score Text").GetComponent<TextMeshProUGUI>();
        if (highScoreText == null) highScoreText = transform.Find("High Score Text").GetComponent<TextMeshProUGUI>();
    }

    /// <summary>
    /// Fade out a heart image and minus 1 currentHeart
    /// </summary>
    [Button]
    public void RecoverHeart()
    {
        if (currentHeart < maxHeart)
        {
            currentHeart++;
            hudHearts[currentHeart - 1].gameObject.SetActive(true);
            Tween.Color(hudHearts[currentHeart - 1], Color.white, tweenDuration, tweenDelay);
        }
    }

    /// <summary>
    /// Fade in a heart image and add 1 currentHeart
    /// </summary>
    [Button]
    public void LostHeart()
    {
        Tween.Color(hudHearts[currentHeart - 1],
                transparent, tweenDuration, tweenDelay, null, Tween.LoopType.None, null, () =>
                {
                    hudHearts[currentHeart - 1].gameObject.SetActive(false);
                    currentHeart--;
                    if (currentHeart <= 0)
                    {
                        GameManager.Instance.EnterResult();
                        currentHeart = 999;
                    }
                }
            );
    }

    public void ResetHearts()
    {
        foreach (Image hudHeart in hudHearts)
        {
            hudHeart.color = Color.white;
            hudHeart.gameObject.SetActive(true);
        }
        currentHeart = maxHeart;
        m_lastMiss = 0;
    }

    public void UpdateHUD(int score, int highscore, int miss)
    {
        highScoreText.SetText("High Score: " + highscore);
        scoreText.SetText("Score: " + score);
        if (GameManager.Instance.GameState == GameState.EndlessMode)
        {
            if (miss > m_lastMiss)
            {
                LostHeart();
            }
            m_lastMiss = miss;
        }
    }
}