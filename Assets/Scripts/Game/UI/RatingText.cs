﻿using Pixelplacement;
using TMPro;
using UnityEngine;

// For rating text behaviour
public class RatingText : MonoBehaviour
{
    public float tweenPosOffset = .1f;
    public float tweenDuration = .5f;
    public float tweenDelay = .1f;

    public TMP_ColorGradient missColor;
    public TMP_ColorGradient badColor;
    public TMP_ColorGradient goodColor;
    public TMP_ColorGradient coolColor;

    // Use this for initialization
    private void Start()
    {
        transform.SetParent(GUIManager.Instance.hudController.transform);
        Tween.Position(transform,
            transform.position + Vector3.up * tweenPosOffset, tweenDuration, tweenDelay,
            null, Tween.LoopType.None,
            () => Tween.ShaderColor(gameObject.GetComponent<MeshRenderer>().material, "_FaceColor", Color.clear, tweenDuration - .1f, tweenDelay),
            () => Destroy(gameObject));
    }

    public RatingText SetRating(Timing timing)
    {
        TextMeshPro tmp = GetComponent<TextMeshPro>();
        string comboString = ScoreManager.Instance.Combo == 0 || timing == Timing.Miss ?
            "" : "<color=white>" + ScoreManager.Instance.Combo.ToString();
        tmp.text = string.Format("{0}\n{1}", timing.ToString(), comboString);
        switch (timing)
        {
            case Timing.Miss:
                tmp.colorGradientPreset = missColor;
                break;

            case Timing.Bad:
                tmp.colorGradientPreset = badColor;
                break;

            case Timing.Good:
                tmp.colorGradientPreset = goodColor;
                break;

            case Timing.Cool:
                tmp.colorGradientPreset = coolColor;
                break;

            default:
                Debug.LogWarning("Unregonized rating was rated");
                break;
        }
        return this;
    }
}