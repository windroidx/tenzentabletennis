﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VolumeSlider : MonoBehaviour {

    public bool isBGMSlider;

    private Slider slider;
    private bool m_isPlaying = false; // For SfxTest method

    private void OnEnable()
    {
        slider = GetComponent<Slider>();

        if (isBGMSlider) {
            slider.value = AudioManager.Instance.bgmVolume;
            slider.onValueChanged.AddListener(SetBgmVolume);
        }
        else {
            slider.value = AudioManager.Instance.sfxVolume;
            slider.onValueChanged.AddListener(SetSfxVolume);
        }
    }

    private void OnDisable()
    {
        slider.onValueChanged.RemoveAllListeners();
    }

    void SetBgmVolume(float volume)
    {
        AudioManager.Instance.ApplyBgmVolume(volume);
    }

    void SetSfxVolume(float volume)
    {
        StartCoroutine(SfxTest("RackethitCenter"));
        AudioManager.Instance.ApplySfxVolume(volume);
    }

    private IEnumerator SfxTest(string sfxName)
    {
        if (!m_isPlaying)
        {
            m_isPlaying = true;
            AudioManager.Instance.PlaySfx(sfxName);
            yield return new WaitForSeconds(.5f);
            m_isPlaying = false;
        }
    }
}
