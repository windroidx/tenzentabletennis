﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class InstructionController : MonoBehaviour
{
    public List<Sprite> spriteList;
    public Image image;
    public Button leftButton;
    public Button rightButton;
    public TextMeshProUGUI pageText;

    private int currentSpriteId = 0;
    private int maxSpriteId;
    private readonly string pageTextFormat = "{0}/{1}";

    private void Awake()
    {
        if (image == null)
        {
            Debug.LogWarning("no Instruction Image reference was found!Try to find in hierarchy");
            image = transform.Find("Instruction Window/Instruction Image").GetComponent<Image>();
        }
        if (pageText == null)
        {
            Debug.LogWarning("no Page Text reference was found!Try to find in hierarchy");
            pageText = transform.Find("Instruction Window/Page/Text").GetComponent<TextMeshProUGUI>();
        }
        if (leftButton == null)
        {
            Debug.LogWarning("no Left Button reference was found!Try to find in hierarchy");
            leftButton = transform.Find("Instruction Window/Left Button").GetComponent<Button>();
        }
        if (rightButton == null)
        {
            Debug.LogWarning("no Left Button reference was found!Try to find in hierarchy");
            rightButton = transform.Find("Instruction Window/Right Button").GetComponent<Button>();
        }
        if (spriteList.Count > 0)
        {
            image.sprite = spriteList[0];
            maxSpriteId = spriteList.Count - 1;
        }
        else
        {
            Debug.Log("No sprite in the Instruction sprite list");
        }
    }

    public void PlusImageId(int plusId)
    {
        currentSpriteId = (int)Mathf.Repeat(currentSpriteId + plusId, maxSpriteId + 1);
        image.sprite = spriteList[currentSpriteId];

        // Off the button if the image list get to the end
        if (leftButton != null && rightButton != null)
        {
            if (currentSpriteId == maxSpriteId) rightButton.interactable = false;
            else if (!rightButton.interactable) rightButton.interactable = true;

            if (currentSpriteId == 0) leftButton.interactable = false;
            else if (!leftButton.interactable) leftButton.interactable = true;

            pageText.text = string.Format(pageTextFormat, currentSpriteId + 1, spriteList.Count);
        }
        else
        {
            Debug.LogWarning("No left/right button reference for switching images, No button will be offed");
        }
    }

    public void ResetImageId()
    {
        currentSpriteId = 0;
        image.sprite = spriteList[currentSpriteId];
        leftButton.interactable = false;
        rightButton.interactable = true;
    }
}