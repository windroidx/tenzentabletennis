﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ResultController : MonoBehaviour
{
    public TextMeshProUGUI difficultyText;
    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI highScoreText;
    public TextMeshProUGUI maxComboText;
    public TextMeshProUGUI highComboText;

    private readonly string resultTextFormat = "{0}:\n<size=150%>{1}";

    public void Awake()
    {
        if (difficultyText == null)
        {
            Debug.LogWarning("difficultyText reference not found, Trying to find in hierarchy");
            difficultyText = transform.Find("Panel/Header/Difficulty/Text").GetComponent<TextMeshProUGUI>();
        }
        if (scoreText == null)
        {
            Debug.LogWarning("scoreText reference not found, Trying to find in hierarchy");
            scoreText = transform.Find("Panel/Result Text/Score Text").GetComponent<TextMeshProUGUI>();
        }
        if(highScoreText == null)
        {
            Debug.LogWarning("highScoreText reference not found, Trying to find in hierarchy");
            highScoreText = transform.Find("Panel/Best Result Text/High Score Text").GetComponent<TextMeshProUGUI>();
        }
        if (maxComboText == null)
        {
            Debug.LogWarning("maxComboText reference not found, Trying to find in hierarchy");
            maxComboText = transform.Find("Panel/Result Text/Max Combo Text").GetComponent<TextMeshProUGUI>();
        }
        if (highComboText == null)
        {
            Debug.LogWarning("highComboText reference not found, Trying to find in hierarchy");
            highComboText = transform.Find("Panel/Best Result Text/High Combo Text").GetComponent<TextMeshProUGUI>();
        }
    }

    public void UpdateResult()
    {
        difficultyText.text = GameManager.Instance.CurrentDifficulty.ToString();
        scoreText.text = string.Format(resultTextFormat, "Score", ScoreManager.Instance.Score);
        highScoreText.text = string.Format(resultTextFormat, "High Score", 
            GameManager.Instance.HighScore[(int)GameManager.Instance.CurrentDifficulty]);
        maxComboText.text = string.Format(resultTextFormat, "Max Combo", ScoreManager.Instance.MaxCombo);
        highComboText.text = string.Format(resultTextFormat, "High Combo", 
            GameManager.Instance.HighCombo[(int)GameManager.Instance.CurrentDifficulty]);
    }
}
