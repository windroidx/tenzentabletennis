﻿using UnityEngine;

// Some of the method was called by Mr. Mo behavior and ball controller

[RequireComponent(typeof(Animator))]
public class MrMoController : MonoBehaviour
{
    public float lookAtSpeed = 8f;

    private Animator animator;
    private Transform _ball;
    private Vector3 ballPos = Vector3.zero;
    private bool lookAtBallState;
    private float lookatweight = 1f;

    private float m_holdAnimSpeed;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        m_holdAnimSpeed = animator.GetFloat("HoldSpeed");
    }

    private void OnAnimatorIK(int layerIndex)
    {
        if (lookAtBallState && _ball != null)
        {
            ballPos = _ball.position;

            lookatweight = Mathf.Lerp(lookatweight, 1.0f, lookAtSpeed * Time.deltaTime);

            // Look at the target ball(ball)
            animator.SetLookAtWeight(lookatweight);
            animator.SetLookAtPosition(ballPos);
        }
        // Back to normal when not in holdState
        else
        {
            lookatweight = Mathf.Lerp(lookatweight, 0.0f, lookAtSpeed * Time.deltaTime);

            animator.SetLookAtWeight(lookatweight);
            animator.SetLookAtPosition(ballPos);
        }
    }

    public void LookAtBall(Transform ball)
    {
        _ball = ball;
        lookAtBallState = true;
    }

    public void ReadyEat()
    {
        animator.SetBool("Hold", true);
    }

    public void GiveUpEat()
    {
        animator.SetBool("Hold", false);
    }

    public void ReceiveBall()
    {
        _ball.parent = animator.GetBoneTransform(HumanBodyBones.LeftHand).Find("Hold");
        _ball.localPosition = Vector3.zero;
    }

    public void EndEat()
    {
        lookAtBallState = false;
        animator.SetBool("Hold", false);
        BallBase _ballController = _ball.GetComponent<BallBase>();
        _ballController.AteByMrMo();
        if (_ballController.AteByMrMo().ballType == BallType.Recover && GameManager.Instance.GameState == GameState.EndlessMode)
        {
            //TODO: put some heart recover happy and unhappy animation here
            GUIManager.Instance.hudController.RecoverHeart();
        }
    }

    public void SetAnimSpeed(float speed)
    {
        animator.SetFloat("PlaySpeed", speed);
    }

    public void SetHoldAnimSpeed(float backSplineDuration)
    {
        animator.SetFloat("HoldSpeed", m_holdAnimSpeed * 1 / backSplineDuration);
    }
}