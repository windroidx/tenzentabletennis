﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;

public class SkyDomeController : MonoBehaviour
{
    public Transform cloud;
    public float cloudRotateSpeed = .1f;

    // Start is called before the first frame update
    void Start()
    {
        if (cloud != null)
            Tween.Rotate(cloud, transform.up * cloudRotateSpeed, Space.Self, .1f, 0.0f, null, Tween.LoopType.Loop);
        else
            cloud = transform.Find("Clouds");
            if(cloud == null)
                Debug.LogWarning("Cloud transform not found in SkyDome");
    }
}
