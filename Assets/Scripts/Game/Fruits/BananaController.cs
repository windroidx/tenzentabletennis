﻿using Pixelplacement;
using UniRx;
using UnityEngine;

public class BananaController : BallBase
{
    public float spinSpeed = 60f;
    public float spinTweenDelay = 0f;

    private System.IDisposable missObserver;
    private int splineTweenID;
    private int rotateTweenID;

    public override BallBase Shot(WaveManager.BallData ballData)
    {
        duration = ballData.duration;
        delay = ballData.delay;
        splines = ballData.splines;
        dirID = ballData.dirID;
        ballType = ballData.ballType;
        startTime = Time.time;
        AudioManager.Instance.PlaySfx("Rackethit" + ballData.dirID);

        RefManager.Instance.MrMo.SetHoldAnimSpeed(duration);

        splineTweenID = Tween.Spline(
            splines[(int)SplineType.Shot], transform, 0, 1, false, duration, delay,
            Tween.EaseOut, Tween.LoopType.None, StartMrMoLookAtSelf, ReturnSplineEnd).targetInstanceID;

        rotateTweenID = Tween.Rotate(
            transform, transform.up * spinSpeed, Space.Self, .1f, spinTweenDelay, null, Tween.LoopType.Loop,ReadyMrMo).targetInstanceID;

        missObserver = ScoreManager.Instance.OnRated
            .Where(rating => rating == Timing.Miss)
            .Subscribe(_ =>
            {
                Missed();
            });

        return this;
    }

    // Call this when the ball was not destroyed before the spline reach 100%
    // (that mean the spline is a ReturnSpline)
    private void ReturnSplineEnd()
    {
        RefManager.Instance.HitButtons[(int)dirID].SetRating(Timing.Cool);
        Tween.Stop(rotateTweenID);
        PassSelfToMrMo();
    }

    private void Missed()
    {
        if (gameObject != null)
        {
            Debug.Log("Banana missed");
            missObserver.Dispose();
            DisableAfterImage();
            RefManager.Instance.MrMo.GiveUpEat();
            Tween.Stop(splineTweenID);
            Tween.Stop(rotateTweenID);
            Rigidbody rb = gameObject.GetComponent<Rigidbody>();
            rb.isKinematic = false;
            rb.useGravity = true;
            rb.AddForce(Random.onUnitSphere * 100f);
            Destroy(gameObject, 2f);
        }
    }

    private void OnDestroy()
    {
        if (missObserver != null) missObserver.Dispose();
    }
}