﻿using Pixelplacement;
using UnityEngine;

public abstract class BallBase : MonoBehaviour
{
    public float backSplineDuration = 1.0f;

    protected float startTime; // use for calculate spline percentage

    [HideInInspector]
    public float duration;

    [HideInInspector]
    public float delay;

    [HideInInspector]
    public Spline[] splines;

    [HideInInspector]
    public DirID dirID;

    [HideInInspector]
    public BallType ballType;

    public virtual BallBase Shot(WaveManager.BallData ballData)
    {
        duration = ballData.duration;
        delay = ballData.delay;
        splines = ballData.splines;
        dirID = ballData.dirID;
        ballType = ballData.ballType;
        startTime = Time.time;
        AudioManager.Instance.PlaySfx("Rackethit" + ballData.dirID);
        Tween.Spline(splines[(int)SplineType.Shot], transform, 0, 1, true, duration, delay,
        Tween.EaseOut, Tween.LoopType.None, StartMrMoLookAtSelf, DestroySelf);

        return this;
    }

    public virtual BallBase Back()
    {
        float hitTime = Time.time - startTime;
        float currentPercentage = hitTime / duration;

        RefManager.Instance.MrMo.SetHoldAnimSpeed(backSplineDuration);

        AudioManager.Instance.PlaySfx("Rackethit" + dirID);
        Tween.Spline(splines[(int)SplineType.Back], transform, currentPercentage, 0, true, backSplineDuration, delay, null, Tween.LoopType.None, ReadyMrMo, PassSelfToMrMo);

        return this;
    }

    public BallBase AteByMrMo()
    {
        Destroy(gameObject);
        return this;
    }

    protected void StartMrMoLookAtSelf()
    {
        RefManager.Instance.MrMo.LookAtBall(transform);
    }

    protected void ReadyMrMo()
    {
        RefManager.Instance.MrMo.ReadyEat();
    }

    protected void PassSelfToMrMo()
    {
        DisableAfterImage();
        RefManager.Instance.MrMo.ReceiveBall();
    }

    protected void DisableAfterImage()
    {
        GetComponent<AfterImageEffects>().EnableAfterImage = false;
    }

    protected void DestroySelf()
    {
        Destroy(gameObject);
    }

    protected virtual void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Table")
        {
            Debug.Log("Touched table");
            AudioManager.Instance.PlaySfx(string.Format("Tablehit{0}", (int)dirID >= 3 ? dirID - 2 : dirID));
        }
    }
}