﻿using System.Collections.Generic;
using UnityEngine;

public class AfterImageEffects : MonoBehaviour
{
    public bool EnableAfterImage;
    public Color AfterImageColor = Color.black;
    public float SurvivalTime = 1;
    public float IntervalTime = 0.2f;

    [Range(0.1f, 1.0f)] public float InitialAlpha = 1.0f;

    public Material AfterImageMat;

    private float time = 0;
    private List<AfterImage> afterImages;
    private SkinnedMeshRenderer[] rens;

    private void Awake()
    {
        afterImages = new List<AfterImage>();
        rens = transform.Find("Model").GetComponentsInChildren<SkinnedMeshRenderer>();
    }

    private void Update()
    {
        if (EnableAfterImage && afterImages != null)
        {
            if (rens == null)
            {
                EnableAfterImage = false;
                return;
            }

            time += Time.deltaTime;
            CreateAfterImage();
            UpdateAfterImage();
        }
    }

    private void CreateAfterImage()
    {
        if (time >= IntervalTime)
        {
            time = 0;

            foreach (SkinnedMeshRenderer ren in rens)
            {
                Mesh mesh = new Mesh();
                ren.BakeMesh(mesh);

                Material material = new Material(AfterImageMat);

                afterImages.Add(new AfterImage(
                    mesh,
                    material,
                    transform.localToWorldMatrix,
                    InitialAlpha,
                    Time.realtimeSinceStartup,
                    SurvivalTime));
            }
        }
    }

    private void UpdateAfterImage()
    {
        for (int i = 0; i < afterImages.Count; i++)
        {
            float passingTime = Time.realtimeSinceStartup - afterImages[i].StartTime;

            if (passingTime > afterImages[i].Duration && afterImages.Count > 1)
            {
                AfterImage afterImage = afterImages[i];
                afterImages.Remove(afterImage);
                Destroy(afterImage);
                continue;
            }

            if (afterImages[i].Material.HasProperty("_Color"))
            {
                //afterImages[i].Alpha *= (1 - passingTime / afterImages[i].Duration);
                AfterImageColor.a = afterImages[i].Alpha * (1 - passingTime / afterImages[i].Duration);

                afterImages[i].Material.SetColor("_Color", AfterImageColor);
            }

            Graphics.DrawMesh(afterImages[i].Mesh, afterImages[i].Matrix, afterImages[i].Material, gameObject.layer);
        }
    }

    internal class AfterImage : Object
    {
        public Mesh Mesh;
        public Material Material;
        public Matrix4x4 Matrix;
        public float Alpha;
        public float StartTime;
        public float Duration;

        public AfterImage(Mesh mesh, Material material, Matrix4x4 matrix4x4, float alpha, float startTime,
            float duration)
        {
            Mesh = mesh;
            Material = material;
            Matrix = matrix4x4;
            Alpha = alpha;
            StartTime = startTime;
            Duration = duration;
        }
    }
}