﻿using UnityEngine;
using System.Collections;

public class ParticleFXSelfDisable : MonoBehaviour
{
    ParticleSystem[] particleFXs;

    private void Awake()
    {
        particleFXs = GetComponentsInChildren<ParticleSystem>();
        StartCoroutine(SelfDisable());
    }

    private void OnEnable()
    {
        foreach (ParticleSystem particleFX in particleFXs)
        {
            particleFX.Play();
        }
        StartCoroutine(SelfDisable());
    }

    private void OnDisable()
    {
        foreach (ParticleSystem particleFX in particleFXs)
        {
            particleFX.Stop();
            particleFX.Clear();
        }
    }

    private IEnumerator SelfDisable()
    {
        float longestDuration = 0;
        
        foreach (ParticleSystem particleFX in particleFXs)
        {
            if (particleFX.main.duration > longestDuration) longestDuration = particleFX.main.duration;
        }
        yield return new WaitForSeconds(longestDuration + .1f);
        gameObject.SetActive(false);
    }
}