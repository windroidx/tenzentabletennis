﻿using UnityEngine;

public class DestroyZoneController : MonoBehaviour {

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Ball") Destroy(other.gameObject);
    }
}
