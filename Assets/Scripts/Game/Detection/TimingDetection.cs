﻿using UnityEngine;

public class TimingDetection : MonoBehaviour
{
    public Timing timingID;

    private BallBase ball;
    private ButtonController buttonParent;
    private DirID dirID;

    private void Start()
    {
        buttonParent = GetComponentInParent<ButtonController>();
        dirID = buttonParent.dirID;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Ball" && ScoreManager.Instance.BallRating == Timing.None)
        {
            ball = other.GetComponent<BallBase>();
            if (ball.dirID == dirID)
            {
                buttonParent.SetBallTiming(ball, timingID);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Ball" && ball != null
            && ScoreManager.Instance.BallRating != Timing.None)
        {
            ball = null;
            buttonParent.SetBallTiming(null, Timing.None);
        }
    }
}